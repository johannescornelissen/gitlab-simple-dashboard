# gitlab-simple-dashboard

Monitor pipelines from groups without any server, just runs in browser.

Configuration values are stored in local storage within the browser.

If no groups are defined a configuration editor is shown.

A menu option is shown in the right top corner to switch between groups and configuration editor.

* define the url to the gitlab api on your server
* create an access token and paste it in the configuration editor
* add groups to show pipelines for like group or group/subgroup..
* groups are scanned recursively for projects with pipelines
 
Use specific groups to lessen the load on the server to retreive project pipelines.

pages are also hosted on: [gitlab.io](https://johannescornelissen.gitlab.io/gitlab-simple-dashboard/)