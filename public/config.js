﻿var dash_config = {
    overall_update_time: 900000, // 15 minutes in milliseconds 15*60*1000
    status_update_time: 10000, // check status every 10 seconds
    gitlab_api: "https://gitlab.com/api/v4", // url to api interface on gitlab server
    gitlab_token: "", // access token to be used to gather info: https://docs.gitlab.com/ee/api/README.html#personal-access-tokens
    gitlab_groups: [] // list of group url's, encoded: https://docs.gitlab.com/ee/api/README.html#namespaced-path-encoding
};
