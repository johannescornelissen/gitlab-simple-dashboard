﻿function initialize() {
    // get dash_config from config.js
    processConfig(dash_config);
}

function processConfig(aConfig) {
    window.gitlab_api = getFromLocalStorageByName("gitlab_api", aConfig.gitlab_api);
    window.gitlab_token = getFromLocalStorageByName("gitlab_token", aConfig.gitlab_token);
    try {
        window.gitlab_groups = JSON.parse(getFromLocalStorageByName("gitlab_groups", JSON.stringify(aConfig.gitlab_groups)));
        if (window.gitlab_groups.length > 0) {
            processGroups();
            window.setInterval(processGroups, aConfig.overall_update_time);
            window.setInterval(updateStatus, aConfig.status_update_time);
        }
        else
            showConfigEditor();
    }
    catch (e) {
        showConfigEditor();
    }
    // start timer for overall update
    window.setInterval(updateTime, 1000);
}

function switchView() {
    if (document.getElementsByClassName("urlform").length !== 0)
        processConfig(dash_config);
    else
        showConfigEditor();
}

function showConfigEditor() {
    // clear groups div
    let groupsDiv = document.getElementById("groups");
    groupsDiv.innerHTML = "";

    // fill groups div with editor form
    let formDiv = addElement(groupsDiv, "div", undefined, "group urlform");

    // fill groups div with examples div
    let foundGrouspAndprojectsDiv = addElement(groupsDiv, "div", undefined, "group foundGrouspAndprojects");

    // title
    addElement(formDiv, "h2", undefined, "urlformElement", "simple gitlab pipeline dashboard");
    addElement(formDiv, "p", undefined, "urlformElement", "all configured values are stored in browser local storage");

    //addElement(formDiv, "span", undefined, "urlformElement", "enter gitlab api path on your server");
    let apiLink = addElement(formDiv, "a", undefined, "urlformElement", "enter gitlab api path on your server");
    apiLink.href = "https://docs.gitlab.com/ee/api/#basic-usage";
    apiLink.setAttribute('target', '_blank');

    let gitlab_api_path_element = addElement(formDiv, "input", undefined, "urlformElement");
    gitlab_api_path_element.type = "text";
    gitlab_api_path_element.value = window.gitlab_api;

    //addElement(formDiv, "span", undefined, "urlformElement", "enter gitlab api token");
    let tokenLink = addElement(formDiv, "a", undefined, "urlformElement", "enter gitlab api token");
    tokenLink.href = "https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html";
    tokenLink.setAttribute('target', '_blank');

    let gitlab_api_token_element = addElement(formDiv, "input", undefined, "urlformElement");
    gitlab_api_token_element.type = "text";
    gitlab_api_token_element.value = window.gitlab_token;

    addElement(formDiv, "span", undefined, "urlformElement urlMaxWidth", "enter gitlab groups, one group per line, each group can be a path like &lt;group&gt;/&lt;subgroup&gt;");
    //if (!window.gitlab_groups || window.gitlab_groups.length === 0)
    //    addElement(formDiv, "span", undefined, "urlformElement noGroupsDefined", " no groups defined");
    let groupsElement = addElement(formDiv, "textarea", undefined, "urlformElement");
    groupsElement.cols = 50;
    groupsElement.rows = 5;
    if (window.gitlab_groups.join && window.gitlab_groups.length > 5)
        groupsElement.rows = window.gitlab_groups.length;
    groupsElement.value = window.gitlab_groups.join ? window.gitlab_groups.join("\n") : window.gitlab_groups;
    groupsElement.value = groupsElement.value.replace(/%2F/g, "/"); // regex to replace ALL %2F with /

    let previewButton = addElement(formDiv, "button", undefined, "urlformElement", "preview groups");
    previewButton.onclick = function (element) {
        foundGrouspAndprojectsDiv.innerHTML = "";
        try {
            let groups = groupsElement.value.replace(/\//g, "%2F").split("\n"); // regex to replace ALL / with % 2F
            if (groups.length > 0) {
                let groupsUL = addElement(foundGrouspAndprojectsDiv, "ul");
                // fill examples div
                for (let g in groups) {
                    fetch(gitlab_api_path_element.value + "/groups/" + groups[g], { headers: { "private-token": gitlab_api_token_element.value } })
                        .then(data => { return data.json(); })
                        .then(groupDetails => {
                            showExampleProjectsInGroup(groupsUL, gitlab_api_path_element.value, gitlab_api_token_element.value, groupDetails.name, groupDetails.description, groups[g]);
                        });
                }
            }
            else
                addElement(foundGrouspAndprojectsDiv, "span", undefined, undefined, "No groups found!");
        }
        catch (e) {
            addElement(foundGrouspAndprojectsDiv, "span", undefined, undefined, "Exception looking up groups: " + e.Message);
        }
    };

    let storeButton = addElement(formDiv, "button", undefined, "urlformElement", "store values in browser local storage");
    storeButton.onclick = function (element) {
        window.localStorage.setItem("gitlab_api", gitlab_api_path_element.value);
        window.localStorage.setItem("gitlab_token", gitlab_api_token_element.value);
        let groups = groupsElement.value.replace(/\//g, "%2F").split("\n"); // regex to replace ALL / with % 2F
        window.localStorage.setItem("gitlab_groups", JSON.stringify(groups));
    };

    addElement(formDiv, "span", undefined, "urlformElement", "tip: click menu button in the right top corner<br>to switch between editor and status view");

    let dashButton = addElement(formDiv, "button", undefined, "urlformElement", "switch to dashboard");
    dashButton.onclick = function (element) {
        processConfig(dash_config);
    };

    addElement(formDiv, "p");

    addElement(formDiv, "span", undefined, "urlformElement urlMaxWidth", "remove all values from local browser storage<br>all configured values will be reverted to their defaults");
    let removeButton = addElement(formDiv, "button", undefined, "urlformElement", "remove values");
    removeButton.onclick = function (element) {
        window.localStorage.removeItem("gitlab_api");
        window.localStorage.removeItem("gitlab_token");
        window.localStorage.removeItem("gitlab_groups");
        window.localStorage.removeItem("groups");
    };
}

function showExampleProjectsInGroup(aParent, aApi, aToken, aGroupName, aGroupDescription, aGroupURL) {
    fetch(aApi + "/groups/" + aGroupURL + "/projects?include_subgroups=true&simple=true&per_page=100", { headers: { "private-token": aToken } })
        .then(data => { return data.json(); })
        .then(projects => {
            showExampleProject(aParent, aApi, aToken, aGroupName, aGroupDescription, projects);
        });
}

function showExampleProject(aParent, aApi, aToken, aGroupName, aGroupDescription, aProjects) {
    let groupLI = addElement(aParent, 'li', undefined, "ellipsis", aGroupName + ": <i>" + aGroupDescription + "</i>");
    let groupUL = addElement(groupLI, "ul");
    for (let i = 0; i < aProjects.length; i++) {
        fetch(aApi + "/projects/" + aProjects[i].id + "/pipelines", { headers: { "private-token": aToken } })
            .then(data => {
                return data.json();
            })
            .then(pipelines => {
                showExampleProjectStatus(groupUL, aProjects[i].id, aProjects[i].description, aProjects[i].path_with_namespace, pipelines);
            })
            .catch(function (error) {
                showExampleProjectStatus(groupUL, aProjects[i].id, aProjects[i].description, aProjects[i].path_with_namespace);
            });
    }
}

function showExampleProjectStatus(aGroupUL, aProjectID, aProjectDescription, aProjectPath, aPipelines) {
    addElement(aGroupUL, 'li', undefined, "ellipsis " + (aPipelines && aPipelines.length > 0 ? aPipelines[0].status : "hasNoPipelines"), aProjectID + ": " + aProjectDescription + "  <i>" + aProjectPath + "</i>");
}

function updateTime() {
    let timeElements = document.getElementsByClassName("timeSpan");
    // fill first
    if (timeElements) {
        for (let i = 0; i < timeElements.length; i++) {
            if (i === 0) {
                let now = new Date();
                timeElements[i].innerHTML =
                    now.getFullYear() + "." + (now.getMonth() + 1).toString().padStart(2, "0") + "." + now.getDate().toString().padStart(2, "0") + "&nbsp;&nbsp;" +
                    now.getHours() + ":" + now.getMinutes().toString().padStart(2, "0") + ":" + now.getSeconds().toString().padStart(2, "0");
            }
            else
                timeElements[i].innerHTML = "";
        }
    }
}

function updateStatus() {
    let pipelineDivs = document.getElementsByClassName("pipeline");
    for (let i = 0; i < pipelineDivs.length; i++)
        fetchProject(pipelineDivs[i].project);
}

function fetchProject(aProject) {
    fetch(window.gitlab_api + "/projects/" + aProject.id + "/pipelines", { headers: { "private-token": window.gitlab_token } })
        .then(data => { return data.json(); })
        .then(pipelines => { aProject.render(pipelines); });
}

function processGroups() {
    // clear groups div
    let groupsDiv = document.getElementById("groups");
    groupsDiv.innerHTML = "";
    // fill groups div
    for (let g in window.gitlab_groups) {
        let page = 1;
        let totalPages = 1;
        while (page <= totalPages) {
            totalPages = processGroup(window.gitlab_groups[g], page);
            page++;
        }
    }
}

async function processGroup(aGroupURL) {
    let response = await fetch(window.gitlab_api + "/groups/" + aGroupURL, { headers: { "private-token": window.gitlab_token } });
    let groupDetails = await response.json();
    let page = 1;
    while (
        await createProjectsInGroup(
            (groupDetails.description && 0 < groupDetails.description.length && groupDetails.description.length <= 25) ? groupDetails.description : groupDetails.name,
            aGroupURL, groupDetails.web_url, groupDetails.avatar_url, page)) {
        page++;
    }
    /*
    fetch(window.gitlab_api + "/groups/" + aGroupURL +"?per_page=100&page={page}", { headers: { "private-token": window.gitlab_token } })
        .then(data => {
            //todo: data.totalPages = get x-total-pages from returned header 
            data.totalPages = 1;
            return data.json();
        })
        .then(groupDetails => {
            createProjectsInGroup((groupDetails.description && 0 < groupDetails.description.length && groupDetails.description.length <= 25) ? groupDetails.description : groupDetails.name, aGroupURL, groupDetails.web_url, groupDetails.avatar_url);
            return groupDetails.totalPages; // todo: return total pages from first request
        });
    */
}

async function createProjectsInGroup(aGroup, aGroupURL, aGroupClickURL, aGroupIconUrl, aPage) {
    let response = await fetch(
        `${window.gitlab_api}/groups/${aGroupURL}/projects?include_subgroups=true&simple=true&per_page=100&page=${aPage}`,
        { headers: { "private-token": window.gitlab_token } });
    //let headers = await response.headers;
    let projects = await response.json();
    let totalPages = response.headers.get("x-total-pages");
    console.log(`${aGroup}, ${aPage}: ${projects.length} (${totalPages})`)
    createProject(aGroup, aGroupURL, aGroupClickURL, aGroupIconUrl, projects);
    return aPage < totalPages; // projects.length == 100;
}

function createProject(aGroup, aGroupURL, aGroupClickURL, aGroupIconUrl, aProjects) {
    for (let i = 0; i < aProjects.length; i++) {
        let project = {};
        project.group = aGroup;
        project.groupURL = aGroupURL;
        project.groupClickURL = aGroupClickURL;
        project.groupIconURL = aGroupIconUrl;
        project.id = aProjects[i].id;
        project.name = aProjects[i].path_with_namespace;
        project.url = aProjects[i].web_url;
        let descr = aProjects[i].description || "";
        project.description = descr.split("\n")[0];
        project.render = renderProject;
        fetchProject(project);
    }
}

function renderProject(aPipelines) {
    if (aPipelines && aPipelines.length > 0) {
        this.newestPipelineID = aPipelines[0].id;
        this.oldestPipelineID = aPipelines[aPipelines.length - 1].id;
        this.lastPipelineStatus = aPipelines[0].status;
        let groupsDiv = document.getElementById("groups");
        let pipelinesDiv = null; // sentinel
        // find/create group
        let groupDiv = document.getElementById(this.group);
        if (!groupDiv) {
            groupDiv = addElement(groupsDiv, "div", this.group, "group");
            sortChildrenOnID(groupsDiv);
            // title
            let titleDiv = addElement(groupDiv, "div", this.group + "-title-div", "groupTitleDiv");
            // todo: for now disable: https://gitlab.com/gitlab-org/gitlab/-/issues/25498
            //if (this.groupIconURL)
            //    addImg(titleDiv, this.group + "-icon", "groupIcon", this.groupIconURL);
            addElement(titleDiv, "span", this.group + "-title", "groupTitle ellipsis", this.group).addEventListener('click',
                function (event) { window.open(this.groupClickURL, '_blank').focus(); }.bind(this));
            // pipelines
            pipelinesDiv = addElement(groupDiv, "div", this.group + "-pipelines", "pipelines");
        }
        else {
            // find pipelinesDiv
            pipelinesDiv = document.getElementById(this.group + "-pipelines");
        }
        // find/create pipeline
        let pipelineDiv = document.getElementById(this.name);
        if (!pipelineDiv) {
            // status: running, pending, success, failed, cancelled, skipped
            // new
            pipelineDiv = addElement(pipelinesDiv, "div", this.name, "pipeline " + this.lastPipelineStatus);
            pipelineDiv.project = this; // store project (this) on div to have the information available during an update
            sortChildrenOnID(pipelinesDiv);
            // title
            let pipelineTitle = addElement(pipelineDiv, "span", this.name + "-description", "pipelineTitle ellipsis", this.description);
            pipelineTitle.title = this.description;
            pipelineTitle.addEventListener('click',
                function (event) { window.open(this.url + "/edit", '_blank').focus(); }.bind(this));
            // name
            addElement(pipelineDiv, "span", this.name + "-name", "pipelineName ellipsis", this.name).addEventListener('click',
                function (event) { window.open(this.url, '_blank').focus(); }.bind(this));
            // history
            let historyDiv = addElement(pipelineDiv, "div", this.name + "-history", "pipelineHistories");
            renderPipelineHistory(this, historyDiv, aPipelines);
            // last pipeline date/time
            this.lastPipelineDateTimeDiv = addElement(pipelineDiv, "div", this.name + "-lastDateTime", "pipelineLastDiv", "searching...");
            this.lastPipelineDateTimeDiv.addEventListener('click',
                function (event) { window.open(this.url + "/pipelines", '_blank').focus(); }.bind(this));
            fetchPipelineDateTimes(this);
        }
        else {
            // update
            pipelineDiv.className = "pipeline " + this.lastPipelineStatus;
            let descriptionSpan = document.getElementById(this.name + "-description");
            descriptionSpan.innerHTML = this.description;
            let historyDiv = document.getElementById(this.name + "-history");
            renderPipelineHistory(this, historyDiv, aPipelines);
            fetchPipelineDateTimes(this);
        }
        // debug output on console
        //console.log(this.group + ": " + this.name + ": " + this.lastPipelineStatus);
    }
}

function fetchPipelineDateTimes(aProject) {
    fetch(window.gitlab_api + "/projects/" + aProject.id + "/pipelines/" + aProject.newestPipelineID, { headers: { "private-token": window.gitlab_token } })
        .then(data => { return data.json(); })
        .then(pipeline => { renderNewestPipelineState(aProject, pipeline); });
}

function renderNewestPipelineState(aProject, aPipeline) {
    if (aProject.lastPipelineDateTimeDiv) {
        let datetime = Date.parse(aPipeline.updated_at);
        let now = new Date();
        let diffInDays = (now - datetime) / (1000 * 60 * 60 * 24);
        // clear contents
        aProject.lastPipelineDateTimeDiv.innerHTML = "";
        // rebuild contents
        addElement(aProject.lastPipelineDateTimeDiv, "span", undefined, "pipelineLastAge", durationStr(diffInDays));
        addElement(aProject.lastPipelineDateTimeDiv, "span", undefined, "pipelineLastUserName", aPipeline.user.name);
        if (diffInDays > 365) {
            if (!document.getElementById(aProject.name + "-removePipeLine"))
                addElement(aProject.lastPipelineDateTimeDiv, "button", aProject.name + "-removePipeLine", "removePipeLineButton", "X").addEventListener('click',
                    function (event) { removePipeLine(this.id, this.oldestPipelineID); }.bind(aProject)); // todo: should be option to remove old jobs
        }
    }
}

function removePipeLine(aProjectID, aPipelineID) {
    console.log(">> remove pipeline with id " + aPipelineID + " from project with id " + aProjectID);
    fetch(window.gitlab_api + "/projects/" + aProjectID + "/pipelines/" + aPipelineID, { method: "DELETE", headers: { "private-token": window.gitlab_token } })
        .then(data => {
            if (data.status === 204) {
                console.log("   remove executed");
                document.location.reload(true); // refresh
            }
            else {
                console.log(" ## remove NOT executed: " + data.status);
            }
        });
}

function durationElement(aNumberOfUnits, aTimeUnit) {
    return aNumberOfUnits >= 1 ? (aNumberOfUnits + ' ' + aTimeUnit + (aNumberOfUnits >= 2 ? 's' : '')) : '';
}

function frac(n) {
    return n - Math.floor(n);
}

function durationStr(aDeltaInDays) {

    if (aDeltaInDays < 1 / (24 * 60 * 60 * 2))
        return '< 1 second';
    else {
        if (aDeltaInDays < 0)
            return '## ERROR, negative duration! (- ' + DurationToStr(-aDeltaInDays) + ')';
        else {
            let Result = "";
            Result += durationElement(Math.trunc(aDeltaInDays), 'day');
            if (aDeltaInDays < 3) {
                Result += (Result.length > 0 ? ' ' : '') + durationElement(Math.trunc(frac(Math.abs(aDeltaInDays)) * 24), 'hour');
                if (aDeltaInDays < 1) {
                    Result += (Result.length > 0 ? ' ' : '') + durationElement(Math.trunc(frac(Math.abs(aDeltaInDays) * 24) * 60), 'minute');
                    if (aDeltaInDays < 1 / 24)
                        Result += (Result.length > 0 ? ' ' : '') + durationElement(Math.round(frac(Math.abs(aDeltaInDays * 24 * 60)) * 60), 'second');
                }
            }
            return Result;
        }
    }
}

function renderPipelineHistory(aProject, aHistoryDiv, aPipelines) {
    let now = new Date();
    let oldIDs = [];
    // clear
    while (aHistoryDiv.firstChild)
        aHistoryDiv.removeChild(aHistoryDiv.firstChild);
    // render
    for (let i = 0; i < aPipelines.length; i++) {
        // aPipelines[i] does not contain updated_at when fetched with general /projects/<id>/pipelines query for <= 12.3
        let datetime = Date.parse(aPipelines[i].updated_at);
        let diffInDays = (now - datetime) / (1000 * 60 * 60 * 24);
        if (diffInDays > 180)
            oldIDs.push(aPipelines[i].id);
        let groupDiv = document.getElementById(aHistoryDiv.id + "-" + aPipelines[i].ref);
        let pipelineJobURL = aPipelines[i].web_url;
        if (!groupDiv) {
            if (aPipelines[i].ref === "master")
                groupDiv = addElement(aHistoryDiv, "div", aHistoryDiv.id + "-" + aPipelines[i].ref, "pipelineHistory " + aPipelines[i].status, undefined, true);
            else
                groupDiv = addElement(aHistoryDiv, "div", aHistoryDiv.id + "-" + aPipelines[i].ref, "pipelineHistory " + aPipelines[i].status);
            addElement(groupDiv, "span", undefined, "dot " + aPipelines[i].status).addEventListener('click',
                function (event) { window.open(pipelineJobURL, '_blank').focus(); }.bind(this));
            let pipelineHistoryRef = addElement(groupDiv, "span", undefined, "pipelineHistoryRef ellipsis", aPipelines[i].ref);
            pipelineHistoryRef.title = aPipelines[i].ref;
            pipelineHistoryRef.addEventListener('click',
                function (event) { window.open(pipelineJobURL, '_blank').focus(); }.bind(this));
            groupDiv.cnt = 0;
        }
        else {
            if (groupDiv.cnt < 3) {
                addElement(groupDiv, "span", undefined, "dot " + aPipelines[i].status).addEventListener('click',
                    function (event) { window.open(pipelineJobURL, '_blank').focus(); }.bind(this));
                groupDiv.cnt += 1;
            }
        }
    }
    // following does not work: see diffInDays above
    // also: groupDiv is not available here!
    /*
    if (oldIDs.length > 0) {
        aProject.oldIDs = oldIDs;
        addElement(groupDiv, "button", aProject.name + "-removePipeLines", "removePipeLineButton", "X").addEventListener('click',
            function (event) {
                for (var i = 0; i < this.oldIDs.length; i++)
                    removePipeLine(this.id, this.oldIDs[i]);
            }.bind(aProject));
    }
    */
}

function addElement(aParent, aType, aID, aClass, aText, aFirst) {
    let element = document.createElement(aType);
    if (aID)
        element.id = aID;
    if (aClass)
        element.className = aClass;
    if (aText)
        element.innerHTML = aText;
    if (aFirst && aParent.firstChild)
        aParent.insertBefore(element, aParent.firstChild);
    else
        aParent.appendChild(element);
    return element;
}

function sortChildrenOnID(aParent) {
    let children = [];
    for (let i = 0; i < aParent.childNodes.length; i++)
        children.push(aParent.childNodes[i]);
    children.sort(function (a, b) {
        let compA = a.getAttribute('id').toUpperCase();
        let compB = b.getAttribute('id').toUpperCase();
        return compA < compB ? -1 : compA > compB ? 1 : 0; // todo: simplefy?
    });
    for (let i = 0; i < children.length; i++)
        aParent.appendChild(children[i]); // appendChild moves child to end of list of children if already exists
}

function addImg(aParent, aID, aClassName, aSrcURL) {
    fetch(aSrcURL, { headers: { "Content-Type": "image/png", "private-token": window.gitlab_token } })
        .then(response => {
            return response.json();
        })
        .then(imageData => {
            let image = document.createElement('img');
            image.src = `data:${imageData.mimeType};base64,${imageData.base64Content}`;
            aParent.insertBefore(image, aParent.firstChild);
        })
        .catch(function (error) {
            // ignore images that throw a fetch error

            let image = document.createElement('img');
            try {
                image.src = aSrcURL + "?private-token=" + window.gitlab_token;
                if (aClassName)
                    image.className = aClassName;
            }
            catch (e) {
                image.src = "";
            }
            aParent.insertBefore(image, aParent.firstChild);

        });
}

function getFromLocalStorageByName(name, def) {
    try {
        if (window.localStorage) {
            let value = window.localStorage.getItem(name);
            return value ? value : def;
        }
        else
            return def;
    }
    catch (e) {
        return def;
    }
}